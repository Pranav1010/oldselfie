<?php
/**
 * version
 * 
 * @package Leemate+
 * @author Gavande
 */

/**
 * Holds the leemate version
 *
 * @global string $leemate_version
 */
$version = '2.0.5';

/**
 * Holds the required PHP version
 *
 * @global string $required_php_version
 */
$required_php_version = '5.3';

/**
 * Holds the required MySQL version
 *
 * @global string $required_mysql_version
 */
$required_mysql_version = '5.0';

?>