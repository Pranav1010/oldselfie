<?php /* Smarty version 3.1.24, created on 2016-05-05 05:11:30
         compiled from "E:/Xampp/htdocs/selfie/content/themes/material/templates/ajax.chat.master.content.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:16697572ad602b84ec9_54528892%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '8a7157d6338e2af49b9f281091038d01aae44008' => 
    array (
      0 => 'E:/Xampp/htdocs/selfie/content/themes/material/templates/ajax.chat.master.content.tpl',
      1 => 1447131540,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '16697572ad602b84ec9_54528892',
  'variables' => 
  array (
    'online_friends' => 0,
    '_user' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.24',
  'unifunc' => 'content_572ad602b8b6e2_14842653',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_572ad602b8b6e2_14842653')) {
function content_572ad602b8b6e2_14842653 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '16697572ad602b84ec9_54528892';
?>
<ul>
    <?php
$_from = $_smarty_tpl->tpl_vars['online_friends']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['_user'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['_user']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['_user']->value) {
$_smarty_tpl->tpl_vars['_user']->_loop = true;
$foreach__user_Sav = $_smarty_tpl->tpl_vars['_user'];
?>
    <li class="feeds-item">
        <div class="data-container clickable small js_chat-start" data-uid="<?php echo $_smarty_tpl->tpl_vars['_user']->value['user_id'];?>
" data-name="<?php echo $_smarty_tpl->tpl_vars['_user']->value['user_fullname'];?>
" data-picture="<?php echo $_smarty_tpl->tpl_vars['_user']->value['user_picture'];?>
">
            <img class="data-avatar" src="<?php echo $_smarty_tpl->tpl_vars['_user']->value['user_picture'];?>
" alt="<?php echo $_smarty_tpl->tpl_vars['_user']->value['user_fullname'];?>
">
            <div class="data-content">
                <div class="pull-right flip">
                    <i class="fa fa-circle"></i>
                </div>
                <div><strong><?php echo $_smarty_tpl->tpl_vars['_user']->value['user_fullname'];?>
</strong></div>
            </div>
        </div>
    </li>
    <?php
$_smarty_tpl->tpl_vars['_user'] = $foreach__user_Sav;
}
?>
</ul><?php }
}
?>