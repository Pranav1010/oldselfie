<?php /* Smarty version 3.1.24, created on 2016-05-19 05:35:00
         compiled from "E:/Xampp/htdocs/selfie/content/themes/material/templates/__feeds_notification.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:4631573d5084b6bc94_13875151%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '35aa69e1f65f3ea665d42c1652406a580056c16f' => 
    array (
      0 => 'E:/Xampp/htdocs/selfie/content/themes/material/templates/__feeds_notification.tpl',
      1 => 1440136170,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '4631573d5084b6bc94_13875151',
  'variables' => 
  array (
    'notification' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.24',
  'unifunc' => 'content_573d5084b77a89_58902168',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_573d5084b77a89_58902168')) {
function content_573d5084b77a89_58902168 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '4631573d5084b6bc94_13875151';
?>
<li class="feeds-item <?php if (!$_smarty_tpl->tpl_vars['notification']->value['seen']) {?>unread<?php }?>" data-id="<?php echo $_smarty_tpl->tpl_vars['notification']->value['notification_id'];?>
">
    <a class="data-container" href="<?php echo $_smarty_tpl->tpl_vars['notification']->value['url'];?>
">
        <img class="data-avatar" src="<?php echo $_smarty_tpl->tpl_vars['notification']->value['user_picture'];?>
" alt="">
        <div class="data-content">
            <div><span class="name"><?php echo $_smarty_tpl->tpl_vars['notification']->value['user_fullname'];?>
</span></div>
            <div><i class="fa <?php echo $_smarty_tpl->tpl_vars['notification']->value['icon'];?>
 pr5"></i> <?php echo $_smarty_tpl->tpl_vars['notification']->value['message'];?>
</div>
            <div class="time js_moment" data-time="<?php echo $_smarty_tpl->tpl_vars['notification']->value['time'];?>
"><?php echo $_smarty_tpl->tpl_vars['notification']->value['time'];?>
</div>
        </div>
    </a>
</li><?php }
}
?>