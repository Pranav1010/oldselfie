<?php /* Smarty version 3.1.24, created on 2016-05-05 10:03:03
         compiled from "E:/Xampp/htdocs/selfie/content/themes/material/templates/ajax.chat.conversation.messages.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:22835572b1a5746ad54_70610538%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'c5aeeb1a5613de6c9d7cb38748995a415c4fcc9b' => 
    array (
      0 => 'E:/Xampp/htdocs/selfie/content/themes/material/templates/ajax.chat.conversation.messages.tpl',
      1 => 1445667296,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '22835572b1a5746ad54_70610538',
  'variables' => 
  array (
    'conversation' => 0,
    'system' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.24',
  'unifunc' => 'content_572b1a57811687_54953714',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_572b1a57811687_54953714')) {
function content_572b1a57811687_54953714 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '22835572b1a5746ad54_70610538';
if ($_smarty_tpl->tpl_vars['conversation']->value['total_messages'] >= $_smarty_tpl->tpl_vars['system']->value['max_results']) {?>
<!-- see-more -->
<div class="alert alert-post see-more small js_see-more" data-id=<?php echo $_smarty_tpl->tpl_vars['conversation']->value['conversation_id'];?>
  data-get="messages">
    <span><?php echo __("Loading Older Messages");?>
</span>
    <div class="loader loader_small x-hidden"></div>
</div>
<!-- see-more -->
<?php }?>

<ul>
    <?php echo $_smarty_tpl->getSubTemplate ('ajax.chat.messages.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('messages'=>$_smarty_tpl->tpl_vars['conversation']->value['messages']), 0);
?>

</ul><?php }
}
?>