<?php /* Smarty version 3.1.24, created on 2016-05-05 06:09:34
         compiled from "E:/Xampp/htdocs/selfie/content/themes/material/templates/__emoji-menu.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:6218572ae39e5eb912_75004135%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '03a4a0ba3771f0671103cdd731627fb37825a9e0' => 
    array (
      0 => 'E:/Xampp/htdocs/selfie/content/themes/material/templates/__emoji-menu.tpl',
      1 => 1433201494,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '6218572ae39e5eb912_75004135',
  'has_nocache_code' => false,
  'version' => '3.1.24',
  'unifunc' => 'content_572ae39e5ec688_52545612',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_572ae39e5ec688_52545612')) {
function content_572ae39e5ec688_52545612 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '6218572ae39e5eb912_75004135';
?>
<div class="emoji-menu">
     <i data-emoji=":)" class="js_emoji twa twa-xlg twa-smile"></i>
     <i data-emoji=":(" class="js_emoji twa twa-xlg twa-worried"></i>
     <i data-emoji=":P" class="js_emoji twa twa-xlg twa-stuck-out-tongue"></i>
     <i data-emoji=":D" class="js_emoji twa twa-xlg twa-smiley"></i>
     <i data-emoji=":O" class="js_emoji twa twa-xlg twa-open-mouth"></i>
     <i data-emoji=";)" class="js_emoji twa twa-xlg twa-wink"></i>
     <i data-emoji=":@" class="js_emoji twa twa-xlg twa-angry"></i>
     <i data-emoji=":/" class="js_emoji twa twa-xlg twa-confused"></i>
     <i data-emoji=";(" class="js_emoji twa twa-xlg twa-sob"></i>
     <i data-emoji="^_^" class="js_emoji twa twa-xlg twa-grin"></i>
     <i data-emoji="B|" class="js_emoji twa twa-xlg twa-sunglasses"></i>
     <i data-emoji="<3" class="js_emoji twa twa-xlg twa-heart"></i>
     <i data-emoji="O:)" class="js_emoji twa twa-xlg twa-innocent"></i>
     <i data-emoji="(devil)" class="js_emoji twa twa-xlg twa-rage"></i>
     <i data-emoji=":S" class="js_emoji twa twa-xlg twa-worried"></i>
     <i data-emoji="*)" class="js_emoji twa twa-xlg twa-kissing-heart"></i>
     <i data-emoji="(y)" class="js_emoji twa twa-xlg twa-thumbsup"></i>
     <i data-emoji="(n)" class="js_emoji twa twa-xlg twa-thumbsdown"></i>
</div><?php }
}
?>