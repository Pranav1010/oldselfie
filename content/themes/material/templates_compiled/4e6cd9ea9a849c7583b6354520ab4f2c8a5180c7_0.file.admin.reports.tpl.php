<?php /* Smarty version 3.1.24, created on 2016-05-05 10:07:58
         compiled from "E:/Xampp/htdocs/selfie/content/themes/material/templates/admin.reports.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:27734572b1b7e8447f0_35231058%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '4e6cd9ea9a849c7583b6354520ab4f2c8a5180c7' => 
    array (
      0 => 'E:/Xampp/htdocs/selfie/content/themes/material/templates/admin.reports.tpl',
      1 => 1452010534,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '27734572b1b7e8447f0_35231058',
  'variables' => 
  array (
    'rows' => 0,
    'row' => 0,
    'system' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.24',
  'unifunc' => 'content_572b1b7e8884c3_29120369',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_572b1b7e8884c3_29120369')) {
function content_572b1b7e8884c3_29120369 ($_smarty_tpl) {
if (!is_callable('smarty_modifier_capitalize')) require_once 'E:/Xampp/htdocs/selfie/includes/libs/smarty/plugins/modifier.capitalize.php';
if (!is_callable('smarty_modifier_date_format')) require_once 'E:/Xampp/htdocs/selfie/includes/libs/smarty/plugins/modifier.date_format.php';

$_smarty_tpl->properties['nocache_hash'] = '27734572b1b7e8447f0_35231058';
?>
<div class="panel panel-default">
    <div class="panel-heading with-icon">
        <i class="material-icons panel-icon">report</i>
        <strong><?php echo __("Reports");?>
</strong>
    </div>
    <div class="panel-body with-table">
        <div class="table-responsive">
            <table class="table table-striped table-bordered table-hover js_dataTable">
                <thead>
                    <tr>
                        <th><?php echo __("ID");?>
</th>
                        <th><?php echo __("Reporter Picture");?>
</th>
                        <th><?php echo __("Reporter Name");?>
</th>
                        <th><?php echo __("Node");?>
</th>
                        <th><?php echo __("Time");?>
</th>
                        <th><?php echo __("Actions");?>
</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
$_from = $_smarty_tpl->tpl_vars['rows']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['row'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['row']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['row']->value) {
$_smarty_tpl->tpl_vars['row']->_loop = true;
$foreach_row_Sav = $_smarty_tpl->tpl_vars['row'];
?>
                    <tr>
                        <td><?php echo $_smarty_tpl->tpl_vars['row']->value['report_id'];?>
</td>
                        <td>
                            <a target="_blank" class="x-image sm" href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/<?php echo $_smarty_tpl->tpl_vars['row']->value['user_name'];?>
" style="background-image:url(<?php echo $_smarty_tpl->tpl_vars['row']->value['user_picture'];?>
);">
                            </a>
                        </td>
                        <td>
                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/<?php echo $_smarty_tpl->tpl_vars['row']->value['user_name'];?>
" target="_blank">
                                <?php echo $_smarty_tpl->tpl_vars['row']->value['user_fullname'];?>

                            </a>
                        </td>
                        <td>
                            <?php echo smarty_modifier_capitalize($_smarty_tpl->tpl_vars['row']->value['node_type']);?>
<br>
                        </td>
                        <td><?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['row']->value['time'],"%e %B %Y");?>
</td>
                        <td>
                            <?php if ($_smarty_tpl->tpl_vars['row']->value['node_type'] == "user") {?>
                                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/admin/users/edit/<?php echo $_smarty_tpl->tpl_vars['row']->value['user_id'];?>
" class="btn btn-xs btn-primary">
                                    <i class="fa fa-pencil"></i>
                                </a>
                            <?php } elseif ($_smarty_tpl->tpl_vars['row']->value['node_type'] == "post") {?>
                                <a class="btn btn-xs btn-info js_open_window" href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/posts/<?php echo $_smarty_tpl->tpl_vars['row']->value['node_id'];?>
" target="_blank">
                                    <i class="fa fa-search"></i>
                                </a>
                                <button class="btn btn-xs btn-danger js_admin-deleter" data-handle="post" data-node="<?php echo $_smarty_tpl->tpl_vars['row']->value['node_id'];?>
" data-id="<?php echo $_smarty_tpl->tpl_vars['row']->value['report_id'];?>
">
                                    <i class="fa fa-trash-o"></i>
                                </button>
                            <?php } elseif ($_smarty_tpl->tpl_vars['row']->value['node_type'] == "comment") {?>
                                <a class="btn btn-xs btn-info js_open_window" href="<?php echo $_smarty_tpl->tpl_vars['row']->value['url'];?>
" target="_blank">
                                    <i class="fa fa-search"></i>
                                </a>
                                <button class="btn btn-xs btn-danger js_admin-deleter" data-handle="comment" data-node="<?php echo $_smarty_tpl->tpl_vars['row']->value['node_id'];?>
" data-id="<?php echo $_smarty_tpl->tpl_vars['row']->value['report_id'];?>
">
                                    <i class="fa fa-trash-o"></i>
                                </button>
                            <?php }?>
                            <button class="btn btn-xs btn-warning js_admin-deleter" data-handle="report" data-id="<?php echo $_smarty_tpl->tpl_vars['row']->value['report_id'];?>
">
                                <i class="fa fa-eye"></i>
                            </button>
                        </td>
                    </tr>
                    <?php
$_smarty_tpl->tpl_vars['row'] = $foreach_row_Sav;
}
?>
                </tbody>
            </table>
        </div>
    </div>
</div><?php }
}
?>