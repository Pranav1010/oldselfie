<?php /* Smarty version 3.1.24, created on 2016-05-05 05:11:30
         compiled from "E:/Xampp/htdocs/selfie/content/themes/material/templates/ajax.chat.master.sidebar.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:13568572ad602b201e3_84705893%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'fad21d3191582a09fa6eca0ac896e725436c82cb' => 
    array (
      0 => 'E:/Xampp/htdocs/selfie/content/themes/material/templates/ajax.chat.master.sidebar.tpl',
      1 => 1452357442,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '13568572ad602b201e3_84705893',
  'variables' => 
  array (
    'sidebar_friends' => 0,
    '_user' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.24',
  'unifunc' => 'content_572ad602b74735_64270919',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_572ad602b74735_64270919')) {
function content_572ad602b74735_64270919 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '13568572ad602b201e3_84705893';
?>
<ul>
    <?php
$_from = $_smarty_tpl->tpl_vars['sidebar_friends']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['_user'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['_user']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['_user']->value) {
$_smarty_tpl->tpl_vars['_user']->_loop = true;
$foreach__user_Sav = $_smarty_tpl->tpl_vars['_user'];
?>
    <li class="feeds-item">
        <div class="data-container clickable small js_chat-start" data-uid="<?php echo $_smarty_tpl->tpl_vars['_user']->value['user_id'];?>
" data-name="<?php echo $_smarty_tpl->tpl_vars['_user']->value['user_fullname'];?>
" data-picture="<?php echo $_smarty_tpl->tpl_vars['_user']->value['user_picture'];?>
">
            <img class="data-avatar" src="<?php echo $_smarty_tpl->tpl_vars['_user']->value['user_picture'];?>
" alt="<?php echo $_smarty_tpl->tpl_vars['_user']->value['user_fullname'];?>
">
            <div class="data-content">
                <div class="pull-right flip">
                    <i class="fa fa-circle <?php if ($_smarty_tpl->tpl_vars['_user']->value['user_is_online']) {?>chat-sidebar-online<?php } else { ?>chat-sidebar-offline<?php }?>"></i>
                </div>
                <div><strong><?php echo $_smarty_tpl->tpl_vars['_user']->value['user_fullname'];?>
</strong></div>
            </div>
        </div>
    </li>
    <?php
$_smarty_tpl->tpl_vars['_user'] = $foreach__user_Sav;
}
?>
</ul><?php }
}
?>