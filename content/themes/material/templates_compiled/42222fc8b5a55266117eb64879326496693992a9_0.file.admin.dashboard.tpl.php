<?php /* Smarty version 3.1.24, created on 2016-05-05 10:05:11
         compiled from "E:/Xampp/htdocs/selfie/content/themes/material/templates/admin.dashboard.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:12316572b1ad79b3726_24117206%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '42222fc8b5a55266117eb64879326496693992a9' => 
    array (
      0 => 'E:/Xampp/htdocs/selfie/content/themes/material/templates/admin.dashboard.tpl',
      1 => 1453199472,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '12316572b1ad79b3726_24117206',
  'variables' => 
  array (
    'system' => 0,
    'insights' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.24',
  'unifunc' => 'content_572b1ad79cd579_01521028',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_572b1ad79cd579_01521028')) {
function content_572b1ad79cd579_01521028 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '12316572b1ad79b3726_24117206';
?>
<div class="panel panel-default">
    <div class="panel-heading with-icon">
        <i class="material-icons panel-icon">cloud</i>
        <strong><?php echo __("Dashboard");?>
</strong>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-sm-7">
                <div class="panel panel-primary">
                    <div class="panel-heading text-center"><strong><?php echo __("New users last week");?>
</strong></div>
                    <div class="panel-body">
                        <div>
                            <canvas class="dashboard-status" id="dashboard-users"></canvas>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-5">
                <div class="panel panel-primary">
                    <div class="panel-heading text-center"><strong><?php echo __("User status");?>
</strong></div>
                    <div class="list-group">
                        <a class="list-group-item" href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/admin/users/online">
                            <span class="badge dashboard-user-status-online"><?php echo $_smarty_tpl->tpl_vars['insights']->value['online'];?>
</span>
                            <i class="fa fa-wifi"></i> <?php echo __("Online");?>

                        </a>
                        <a class="list-group-item" href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/admin/users">
                            <span class="badge dashboard-user-status-users"><?php echo $_smarty_tpl->tpl_vars['insights']->value['users'];?>
</span>
                            <i class="fa fa-user"></i> <?php echo __("Users");?>

                        </a>
                        <a class="list-group-item">
                            <span class="badge dashboard-user-status-males"><?php echo $_smarty_tpl->tpl_vars['insights']->value['males'];?>
</span>
                            <i class="fa fa-male"></i> <?php echo __("Males");?>

                        </a>
                        <a class="list-group-item">
                            <span class="badge dashboard-user-status-females"><?php echo $_smarty_tpl->tpl_vars['insights']->value['females'];?>
</span>
                            <i class="fa fa-female"></i> <?php echo __("Females");?>

                        </a>
                        <a class="list-group-item" href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/admin/users/banned">
                            <span class="badge dashboard-user-status-baned"><?php echo $_smarty_tpl->tpl_vars['insights']->value['banned'];?>
</span>
                            <i class="fa fa-minus-circle"></i> <?php echo __("Banned");?>

                        </a>
                        <a class="list-group-item" href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/admin/users">
                            <span class="badge dashboard-user-status-not_activate"><?php echo $_smarty_tpl->tpl_vars['insights']->value['not_activated'];?>
</span>
                            <i class="fa fa-envelope"></i> <?php echo __("Not Activated");?>

                        </a>
                    </div>
                </div>
                
            </div>
        </div>

        <div class="row">
            <div class="col-sm-6">
                <div class="stat-panel success">
                    <div class="stat-cell">
                        <i class="fa fa-newspaper-o bg-icon"></i>
                        <span class="text-xlg dashboard-user-status-posts"><?php echo $_smarty_tpl->tpl_vars['insights']->value['posts'];?>
</span><br>
                        <span class="text-bg"><?php echo __("Posts");?>
</span><br>
                        <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/admin/reports"><?php echo __("Manage Reports");?>
</a>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="stat-panel success">
                    <div class="stat-cell">
                        <i class="fa fa-comments bg-icon"></i>
                        <span class="text-xlg dashboard-user-status-comments"><?php echo $_smarty_tpl->tpl_vars['insights']->value['comments'];?>
</span><br>
                        <span class="text-bg"><?php echo __("Comments");?>
</span><br>
                        <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/admin/reports"><?php echo __("Manage Reports");?>
</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <div class="stat-panel">
                    <div class="stat-cell">
                        <i class="fa fa-flag bg-icon"></i>
                        <span class="text-xlg dashboard-user-status-pages"><?php echo $_smarty_tpl->tpl_vars['insights']->value['pages'];?>
</span><br>
                        <span class="text-bg"><?php echo __("Pages");?>
</span><br>
                        <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/admin/pages"><?php echo __("Manage Pages");?>
</a>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="stat-panel">
                    <div class="stat-cell">
                        <i class="fa fa-users bg-icon"></i>
                        <span class="text-xlg dashboard-user-status-groups"><?php echo $_smarty_tpl->tpl_vars['insights']->value['groups'];?>
</span><br>
                        <span class="text-bg"><?php echo __("Groups");?>
</span><br>
                        <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/admin/groups"><?php echo __("Manage Groups");?>
</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <div class="stat-panel info">
                    <div class="stat-cell">
                        <i class="fa fa-comments-o bg-icon"></i>
                        <span class="text-xlg dashboard-user-status-messages"><?php echo $_smarty_tpl->tpl_vars['insights']->value['messages'];?>
</span><br>
                        <span class="text-bg"><?php echo __("Messages");?>
</span><br>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="stat-panel info">
                    <div class="stat-cell">
                        <i class="fa fa-globe bg-icon"></i>
                        <span class="text-xlg dashboard-user-status-notifications"><?php echo $_smarty_tpl->tpl_vars['insights']->value['notifications'];?>
</span><br>
                        <span class="text-bg"><?php echo __("Notifications");?>
</span><br>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div><?php }
}
?>