<?php /* Smarty version 3.1.24, created on 2016-05-19 05:37:40
         compiled from "E:/Xampp/htdocs/selfie/content/themes/material/templates/__feeds_post.comment.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:26538573d5124d98b14_25372749%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '6cbd33cd46b0c79fdab2231f194566a9edfac890' => 
    array (
      0 => 'E:/Xampp/htdocs/selfie/content/themes/material/templates/__feeds_post.comment.tpl',
      1 => 1450854788,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '26538573d5124d98b14_25372749',
  'variables' => 
  array (
    'comment' => 0,
    'user' => 0,
    'post' => 0,
    'photo' => 0,
    'system' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.24',
  'unifunc' => 'content_573d5124de5ad0_58258618',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_573d5124de5ad0_58258618')) {
function content_573d5124de5ad0_58258618 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '26538573d5124d98b14_25372749';
?>
<li>
    <div class="comment" data-id="<?php echo $_smarty_tpl->tpl_vars['comment']->value['comment_id'];?>
">
        <div class="comment-avatar">
            <a class="comment-avatar-picture" href="<?php echo $_smarty_tpl->tpl_vars['comment']->value['comment_author_url'];?>
" style="background-image:url(<?php echo $_smarty_tpl->tpl_vars['comment']->value['comment_picture'];?>
);">
            </a>
        </div>
        <div class="comment-data">
            <?php if ($_smarty_tpl->tpl_vars['user']->value->_logged_in) {?>
                <?php if ($_smarty_tpl->tpl_vars['comment']->value['user_type'] == "user") {?>
                    <?php if ($_smarty_tpl->tpl_vars['comment']->value['user_id'] == $_smarty_tpl->tpl_vars['user']->value->_data['user_id'] || $_smarty_tpl->tpl_vars['post']->value['user_id'] == $_smarty_tpl->tpl_vars['user']->value->_data['user_id'] || $_smarty_tpl->tpl_vars['photo']->value['user_id'] == $_smarty_tpl->tpl_vars['user']->value->_data['user_id'] || $_smarty_tpl->tpl_vars['comment']->value['post']['user_id'] == $_smarty_tpl->tpl_vars['user']->value->_data['user_id']) {?>
                        <div class="comment-btn">
                            <button type="button" class="close js_delete-comment" data-toggle="tooltip" data-placement="top" title="<?php echo __("Delete");?>
">
                                <span aria-hidden="true">&times;</span>
                            </button>

                        </div>
                    <?php } else { ?>
                        <div class="comment-btn">
                            <button type="button" class="close js_report-comment" data-toggle="tooltip" data-placement="top" title="<?php echo __("Report");?>
">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    <?php }?>
                <?php } elseif ($_smarty_tpl->tpl_vars['comment']->value['user_type'] == "page") {?>
                    <?php if ($_smarty_tpl->tpl_vars['post']->value['page_admin'] == $_smarty_tpl->tpl_vars['user']->value->_data['user_id'] || $_smarty_tpl->tpl_vars['photo']->value['page_admin'] == $_smarty_tpl->tpl_vars['user']->value->_data['user_id'] || $_smarty_tpl->tpl_vars['comment']->value['post']['is_page_admin']) {?>
                        <div class="comment-btn">
                            <button type="button" class="close js_delete-comment" data-toggle="tooltip" data-placement="top" title="<?php echo __("Delete");?>
">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    <?php } else { ?>
                        <div class="comment-btn">
                            <button type="button" class="close js_report-comment" data-toggle="tooltip" data-placement="top" title="<?php echo __("Report");?>
">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    <?php }?>
                <?php }?>
            <?php }?>
            <div class="mb5">
                <span class="text-semibold js_user-popover" data-type="<?php echo $_smarty_tpl->tpl_vars['comment']->value['user_type'];?>
" data-uid="<?php echo $_smarty_tpl->tpl_vars['comment']->value['user_id'];?>
">
                    <a href="<?php echo $_smarty_tpl->tpl_vars['comment']->value['comment_author_url'];?>
" ><?php echo $_smarty_tpl->tpl_vars['comment']->value['comment_author_name'];?>
</a>
                </span>
                <?php if ($_smarty_tpl->tpl_vars['comment']->value['comment_author_verified']) {?>
                <i data-toggle="tooltip" data-placement="top" title="<?php echo __("Verified profile");?>
" class="fa fa-check verified-badge"></i>
                <?php }?>
                <?php echo $_smarty_tpl->tpl_vars['comment']->value['text'];?>

                <?php if ($_smarty_tpl->tpl_vars['comment']->value['image'] != '') {?>
                    <span class="text-link js_lightbox-nodata" data-image="<?php echo $_smarty_tpl->tpl_vars['comment']->value['image'];?>
">
                        <img alt="" class="img-responsive" src="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_uploads'];?>
/<?php echo $_smarty_tpl->tpl_vars['comment']->value['image'];?>
">
                    </span>
                <?php }?>
            </div>
            <div>
                <span class="text-muted js_moment" data-time="<?php echo $_smarty_tpl->tpl_vars['comment']->value['time'];?>
"><?php echo $_smarty_tpl->tpl_vars['comment']->value['time'];?>
</span>
                · 
                <?php if ($_smarty_tpl->tpl_vars['comment']->value['i_like']) {?>
                <span class="text-link js_unlike-comment"><?php echo __("Unlike");?>
</span>
                <?php } else { ?>
                <span class="text-link js_like-comment"><?php echo __("Like");?>
</span>
                <?php }?>
                <span class="js_comment-likes <?php ob_start();
echo $_smarty_tpl->tpl_vars['comment']->value['likes'];
$_tmp1=ob_get_clean();
if ($_tmp1 == 0) {?>x-hidden<?php }?>">
                    · 
                    <span class="text-link" data-toggle="modal" data-url="posts/who_likes.php?comment_id=<?php echo $_smarty_tpl->tpl_vars['comment']->value['comment_id'];?>
"><i class="fa fa-thumbs-o-up"></i> <span class="js_comment-likes-num"><?php echo $_smarty_tpl->tpl_vars['comment']->value['likes'];?>
</span></span>
                </span>
            </div>
        </div>
    </div>
</li><?php }
}
?>