<?php /* Smarty version 3.1.24, created on 2016-05-19 05:41:38
         compiled from "E:/Xampp/htdocs/selfie/content/themes/material/templates/__feeds_photo.comments.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:3597573d5212906c90_60534751%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'd5b421e6854e7f0e98cb3f5d21d3a0f9ee1a9b5b' => 
    array (
      0 => 'E:/Xampp/htdocs/selfie/content/themes/material/templates/__feeds_photo.comments.tpl',
      1 => 1445052450,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '3597573d5212906c90_60534751',
  'variables' => 
  array (
    'photo' => 0,
    'system' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.24',
  'unifunc' => 'content_573d5212928a24_72235815',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_573d5212928a24_72235815')) {
function content_573d5212928a24_72235815 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '3597573d5212906c90_60534751';
?>
<div class="post-comments">

    <!-- previous comments -->
    <?php if ($_smarty_tpl->tpl_vars['photo']->value['comments'] >= $_smarty_tpl->tpl_vars['system']->value['max_results']) {?>
    <div class="pb10 js_see-more" data-get="photo_comments" data-id="<?php echo $_smarty_tpl->tpl_vars['photo']->value['photo_id'];?>
" data-remove="true">
        <span class="text-link">
            <i class="fa fa-comment-o"></i>
            <?php echo __("View previous comments");?>

        </span>
        <div class="loader loader_small x-hidden"></div>
    </div>
    <?php }?>
    <!-- previous comments -->

    <!-- comments -->
    <ul>
        <?php if ($_smarty_tpl->tpl_vars['photo']->value['comments'] > 0) {?>
            <?php
$_from = $_smarty_tpl->tpl_vars['photo']->value['photo_comments'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['comment'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['comment']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['comment']->value) {
$_smarty_tpl->tpl_vars['comment']->_loop = true;
$foreach_comment_Sav = $_smarty_tpl->tpl_vars['comment'];
?>
            <?php echo $_smarty_tpl->getSubTemplate ('__feeds_post.comment.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>

            <?php
$_smarty_tpl->tpl_vars['comment'] = $foreach_comment_Sav;
}
?>
        <?php }?>
    </ul>
    <!-- comments -->

    <!-- post a comment -->
    <?php echo $_smarty_tpl->getSubTemplate ('__feeds_post.comment_form.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('_handle'=>'photo','_id'=>$_smarty_tpl->tpl_vars['photo']->value['photo_id']), 0);
?>

    <!-- post a comment -->
    
</div><?php }
}
?>