<?php /* Smarty version 3.1.24, created on 2016-05-05 06:09:34
         compiled from "E:/Xampp/htdocs/selfie/content/themes/material/templates/ajax.who_shares.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:5881572ae39e4455c4_13785340%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '76bec040b2074af3f8a377a82850b5d3f90bd7ac' => 
    array (
      0 => 'E:/Xampp/htdocs/selfie/content/themes/material/templates/ajax.who_shares.tpl',
      1 => 1447571602,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '5881572ae39e4455c4_13785340',
  'variables' => 
  array (
    'posts' => 0,
    'system' => 0,
    'id' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.24',
  'unifunc' => 'content_572ae39e478997_70061877',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_572ae39e478997_70061877')) {
function content_572ae39e478997_70061877 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '5881572ae39e4455c4_13785340';
?>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h5 class="modal-title"><?php echo __("People Who Shared This");?>
</h5>
</div>
<div class="modal-body">
    <ul>
        <?php
$_from = $_smarty_tpl->tpl_vars['posts']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['post'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['post']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['post']->value) {
$_smarty_tpl->tpl_vars['post']->_loop = true;
$foreach_post_Sav = $_smarty_tpl->tpl_vars['post'];
?>
        <?php echo $_smarty_tpl->getSubTemplate ('__feeds_post.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('_snippet'=>true), 0);
?>

        <?php
$_smarty_tpl->tpl_vars['post'] = $foreach_post_Sav;
}
?>
    </ul>

    <?php if (count($_smarty_tpl->tpl_vars['posts']->value) >= $_smarty_tpl->tpl_vars['system']->value['max_results']) {?>
    <!-- see-more -->
    <div class="alert alert-info see-more js_see-more" data-get="shares" data-id="<?php echo $_smarty_tpl->tpl_vars['id']->value;?>
">
        <span><?php echo __("See More");?>
</span>
        <div class="loader loader_small x-hidden"></div>
    </div>
    <!-- see-more -->
    <?php }?>
    
</div>
<?php }
}
?>